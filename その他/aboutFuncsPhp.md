# 共通事項を共通化する！

- プログラミングでは、複数回出てくるようなコードは、共通化することが重要です。
- 以下、共通化してコードをスッキリさせましょう。

## `function h()`の共通化。

- 今後、`function h()`は様々なところでの利用が想定されます。
- 以下共通化してみましょう。

1. `function h()`を、共通化ファイルである`funcs.php`にコピペ。
![111](/uploads/57c29b7f6effaeebb9fb20e2e708fd88/111.png)

2. この`function h()`を利用したいファイルにて、`require_once('funcs.php');`と記述し、`funcs.php`を呼び出す。

![２２２](/uploads/0191e36e659b50632433b25719dea94b/２２２.png)

3. 利用したいファイルに記述してある`function h()`を削除。

![３３３](/uploads/f59255548595763a71b92e9bd4300516/３３３.png)

- `require_once('funcs.php');`で呼び出すことで、`function h()`を書かなくても`function h()`が動作することを確認してください。
