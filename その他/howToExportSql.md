# 　SQLエクスポートの方法

## PHPMyAdminへ移動
![111](/uploads/5cdb809ab6fbe013c52b35e60d648605/111.png)

## 作成 / 利用したDBを選択して、エクスポート
![222](/uploads/238be65dc3478effd10a96dd4dd7cbc2/222.png)

## エクスポート方法　>　簡易で、特にいじらずにそのまま実行
![333](/uploads/fa9b4f6ff7b4776828b466a2df5792c1/333.png)

## 出来たファイルを共有してください。中身はいじらないでください。
![444](/uploads/17ec800fe18842f7787e517ecc2309fd/444.png)
