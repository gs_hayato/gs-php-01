## firebase removeについて

HTMLに削除ボタンを追加して、そのボタンを押したら指定のデータを消す方法
以下の手順でいけると思います。
※あくまでも参考まで。あくまでも消し方だけ。

- 参考サイト
  - [Firebase CRUD – Create Update and Delete](https://infotechwar.com/programming/firebase-crud-create-update-and-delete/)
### 以下でできること。
削除ボタンを設置。それを押したら、特定のIDのデータが一つだけ削除される。

### 注意
- 授業で配布されたサンプルのコードに追記すれば、削除が体験できるようになっています。
- 以下はあくまでもサンプルなので、完璧な動きではないと思います。

### 手順

- `import { getDatabase,ref,push,set,...` の中に、`remove`を（もしなければ）追加

- html内に、適当に以下のようなremoveボタン追加

```html
-(省略)-

<div>
    <textarea name="" id="text" cols="30" rows="10"></textarea>
    <button id="send">送信</button>
</div>
<button id="remove">remove</button>
<div id="output"></div>

-(以下省略)-
```

- `jQuery`の`$("#send").on("click",function.....`の近くに以下追加

```js

    // ↓下のXXXXXXXXXには、消したいIDを適当に１つ入れてください.多分"-MqTmjSD2bBWwLaXbWnY"みたいなやつです。
    // 今回の例では、ここに記述したIDのやつを１個だけ消します。
    const random_id = "XXXXXXXXX";
    $("#remove").on("click", function () {
        remove(ref(db, "chat/" + random_id));
        console.log("remove: ${random_id}");
    });

```

- firebaseのサイト見てみてください。指定したrandom_idのデータが消えていると思います。
